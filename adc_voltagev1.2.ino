#include <LowPower.h>
#include <Wire.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include<avr/wdt.h>
#include <LiquidCrystal.h>

#define getVoltage(result) ((result*voltageIndex))
#define getCurrent(result) (((result-2500)/40)-0.04)

#define R1 8
#define R2 9


const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

/*WDRCSR 8-bit register for watchdog
 * [7] WDIF  interrupt flag
 * [6] WDIE enabling the interrupt to include your last dying wish!
 * [5] WDCE There isnʼt really any logic behind this, you just have to set WDCE and WDE
       to ʻ1ʼ to enter a sort of ʻsetup modeʼ in the watchdog timer
 * [4] WDIO enable system reset on time-out whenever the watchdog timer times out, it will be reset.
 * [3][2][1][0] WDP0~3 for setting up the timer
 * 
 * wp3~0 - time-out in ms
    0 0 0 0 16
    0 0 0 1 32
    0 0 1 0 64
    0 0 1 1 125
    0 1 0 0 250
    0 1 0 1 500
    0 1 1 0 1000
    0 1 1 1 2000
    1 0 0 0 4000
    1 0 0 1 8000
    wdt_reset(); will reset the watchdog time-out.
 
 WDTCSR configuration:
 WDIE = 1: Interrupt Enable
 WDE = 1 :Reset Enable
 See table for time-out variations:
 WDP3 = 0 :For 1000ms Time-out
 WDP2 = 1 :For 1000ms Time-out
 WDP1 = 1 :For 1000ms Time-out
 WDP0 = 0 :For 1000ms Time-out
 */
 
/*it is a index for raw value of ADC which should be multiplied with value of ADC 
 * to get real value of vlotage
 */

int result=0;
const PROGMEM float voltageIndex=4.209;
byte rawData[4];
const PROGMEM byte slaveAddr=0x13;
int regAddr;
byte command;
byte respond[2];
float voltageValue=0;
double currentValue=0;
float Current,Voltage;
int moduleResetCounter=0;


void resetModules();
void write2LCD();
int R1_on(void);
int R2_on(void);
int R1_off(void);
int R2_off(void);
uint8_t R1Status(void);
uint8_t R2Status(void);
void resetMe(void);

byte Block[8]={
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b11111,

};

void setup() {
  //Serial.begin(9600);
  lcd.begin(16, 2);
  delay(2);
  lcd.clear();
  lcd.clear();
  lcd.createChar(0,Block);
  
  pinMode(R1,OUTPUT);
  pinMode(R2,OUTPUT);
  digitalWrite(R1,HIGH);
  digitalWrite(R2,HIGH);
  
  wdt_disable();
  noInterrupts();
  //initialize timer 1
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;

  //OCR1A = 31250;            // compare match register 16MHz/256/2Hz
  OCR1A = 62499;            // compare match register 16MHz/256/1Hz
  TCCR1B |= (1 << WGM12);   // CTC mode
  TCCR1B |= (1 << CS12);    // 256 prescaler 
  TIMSK1 |= (1 << OCIE1A);  // enable timer compare interrupt
  interrupts();             // enable all interrupts
  //Serial.println("Booting");
  Wire.begin(slaveAddr);
  Wire.onReceive(receiveRegister); // register receive listener below
  Wire.onRequest(respondData); // register respond listener below
  setWDT(0b00001000);  //enable reset for watchdog 
}


void loop() {

  //LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
    LowPower.idle(SLEEP_FOREVER, ADC_OFF, TIMER2_OFF, TIMER1_ON, TIMER0_ON, SPI_OFF, USART0_ON, TWI_ON);
}

// Setting up watchdog timer
void setWDT(byte sWDT) {
   WDTCSR |= 0b00011000;
   WDTCSR = sWDT |  WDTO_2S; //Set WDT based user setting and for 2 second interval
   wdt_reset();
}




ISR(TIMER1_COMPA_vect)          // timer compare interrupt service routine
{
    moduleResetCounter++;
    if(moduleResetCounter==10){
      moduleResetCounter=0;
      resetModules();
    }

    cli();
    write2LCD();
    wdt_reset();
    sei();
      
}

void write2LCD(){
    lcd.clear();
    lcd.print(Voltage); //voltageValue
    lcd.setCursor(5,0);
    lcd.print("VDC");
    lcd.setCursor(8,0);
    lcd.write(byte(0));
    lcd.setCursor(9,0);
    lcd.print(Current); //currentValuerawCurrent
    lcd.setCursor(14,0);
    lcd.print("A");
    lcd.setCursor(0,1);
    lcd.print("R1: ");
    lcd.setCursor(4,1);
    char *R1Status;
    R1Status=(char*)(digitalRead(R1)==1)?"OFF":"ON";
    lcd.print(R1Status);
    lcd.setCursor(8,1);
    lcd.print("R2: ");
    lcd.setCursor(12,1);
    char *R2Status;
    R2Status=(char*)(digitalRead(R2)==1)?"OFF":"ON";
    lcd.print(R2Status);
}

void receiveRegister(int x){ // handler for I2c communication called when data available

//Serial.println(x);
if (x==3){
byte i=0;
while(Wire.available()){
  rawData[i]=Wire.read();
  //Serial.println(rawData[i]);
  i++;
}

if(rawData[0]==1){
  voltageValue=((rawData[1]<<4)|(rawData[2]>>4))*0.003;  //3 becuase we have 3mv steps on ADS module
  Voltage=getVoltage(voltageValue);
}
else if(rawData[0]==2){
  //Serial.println(rawData[1],HEX);
  //Serial.println(rawData[2],HEX);
  currentValue=((rawData[1]<<4)|(rawData[2]))*3;     //3 becuase we have 3mv steps on ADS module
  //Serial.println(currentValue,HEX);
  Current=getCurrent(currentValue);
}

}
else if(x==1){
  //Serial.println("in command mode");
  command=Wire.read();
  //Serial.println(command);
}
}

void resetModules(){
    cli();
    lcd.clear();
    Wire.end();
    delay(10);
    lcd.begin(16, 2);
    Wire.begin(slaveAddr);
    delay(1);
    sei();
}

void respondData(){// handler that is called on response
  int result;
  switch (command){
      
    case 0x91:
      //byte result1;
      result=R1_on();
      Wire.write((byte *)&result,sizeof(result));
      break;

    case 0x95:
      //byte result;
      result=R2_on();
      Wire.write((byte *)&result,sizeof(result));
      break;

    case 0x9a:
      //byte result;
      result=R1_off();
      Wire.write((byte *)&result,sizeof(result));
      break;

     case 0x9e:
      result=R2_off();
      Wire.write((byte *)&result,sizeof(result));
      break;

     case 0x2a:
      result=R1Status();
      Wire.write((byte *)&result,sizeof(result));
      break;

     case 0x2b:
      result=R2Status();
      Wire.write((byte *)&result,sizeof(result));
      break;

     case 0xaa:
      resetMe();
      break;
  }
  
}
//Relay are low acitve
int R1_on(void){
  digitalWrite(R1,LOW);
  return 1;
}

int R2_on(void){
  digitalWrite(R2,LOW);
  return 1;
}

int R1_off(void){
  digitalWrite(R1,HIGH);
  return 1;
}

int R2_off(void){
  digitalWrite(R2,HIGH);
  return 1;
}

uint8_t R1Status(void){
  uint8_t value;
  value=digitalRead(R1);
  return value;
}

uint8_t R2Status(void){
  uint8_t value;
  value=digitalRead(R2);
  return value;
}

void resetMe(){
  cli();
  while(true);
}

